<?php

namespace Drupal\tests\menu_disallow_external_links\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\menu_link_content\Entity\MenuLinkContent;
use Drupal\system\Entity\Menu;

/**
 * Test permissions for module menu_disallow_external_links.
 *
 * @group menu_disallow_external_links
 */
class MenuDisallowExternalLinksTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'menu_disallow_external_links',
    'menu_ui',
  ];

  /**
   * Test user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $user;

  /**
   * Test menu.
   *
   * @var \Drupal\system\Entity\Menu
   */
  protected $menu;

  /**
   * Test menu item.
   *
   * @var \Drupal\menu_link_content\Entity\MenuLinkContent
   */
  protected $menuItem;

  /**
   * Sample list of external links.
   *
   * @var array
   */
  protected $externalLinks = [
    'https://www.drupal.org',
    'http://localhost/',
  ];

  /**
   * Sample list of internal links.
   *
   * @var array
   */
  protected $internalLinks = [
    '/node/add',
    '/user',
    '<front>',
    '<nolink>',
    '<button>',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create test menu.
    $this->menu = Menu::create([
      'id' => 'test',
      'label' => 'Test menu',
    ]);
    $this->menu->save();

    // Create test menu item.
    $this->menuItem = MenuLinkContent::create([
      'title' => 'Test menu item',
      'link' => ['uri' => 'route:<nolink>'],
      'menu_name' => $this->menu->id(),
    ]);
    $this->menuItem->save();

    // Create test user and sign in.
    $this->user = $this->drupalCreateUser(['administer menu']);
    $this->drupalLogin($this->user);
  }

  /**
   * Tests checkbox 'Disallow external links' when editing menus.
   */
  public function testAdminCheckbox() {
    // Checkbox should show up and be unchecked by default.
    $this->drupalGet('admin/structure/menu/manage/' . $this->menu->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->fieldExists('disallow_external_links');
    $this->assertSession()->pageTextContains('Disallow external links');
    $this->assertSession()->pageTextContains('Check this box if this menu should not contain external links.');
    $this->assertSession()->checkboxNotChecked('disallow_external_links');

    // Checkbox should stick.
    $this->submitForm(['disallow_external_links' => TRUE], 'Save');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->checkboxChecked('disallow_external_links');
  }

  /**
   * Menus that allow external links should accept external links.
   */
  public function testExternalLinksAllowed() {
    // Configure menu to allow external links.
    $this->drupalGet('admin/structure/menu/manage/' . $this->menu->id());
    $this->submitForm(['disallow_external_links' => FALSE], 'Save');

    // User *should* be able to create/edit menu items with external links.
    foreach ($this->externalLinks as $link) {
      $this->assertMenuLinkAllowed($link);
    }

    // User *should* be able to create/edit menu items with internal links.
    foreach ($this->internalLinks as $link) {
      $this->assertMenuLinkAllowed($link);
    }
  }

  /**
   * Menus that disallow external links should reject external links.
   */
  public function testExternalLinksDisallowed() {
    // Configure menu to disallow external links.
    $this->drupalGet('admin/structure/menu/manage/' . $this->menu->id());
    $this->submitForm(['disallow_external_links' => TRUE], 'Save');

    // User should *not* be able to create/edit menu items with external links.
    foreach ($this->externalLinks as $link) {
      $this->assertMenuLinkDisallowed($link);
    }

    // User *should* be able to create/edit menu items with internal links.
    foreach ($this->internalLinks as $link) {
      $this->assertMenuLinkAllowed($link);
    }
  }

  /**
   * Asserts that creating/editing a menu item with a given link is allowed.
   *
   * @param string $link
   *   Link of the menu item (URL, path, <nolink, <front>, etc).
   */
  public function assertMenuLinkAllowed($link) {
    $paths = [
      // Add menu item.
      'admin/structure/menu/manage/' . $this->menu->id() . '/add',
      // Edit existing menu item.
      'admin/structure/menu/item/' . $this->menuItem->id() . '/edit',
    ];
    foreach ($paths as $path) {
      // Assert creating menu link with $link is allowed.
      $this->drupalGet($path);
      $this->submitForm([
        'title' => 'My menu item',
        'link[0][uri]' => $link,
      ], 'Save');
      $this->assertSession()->statusCodeEquals(200);
      $this->assertSession()->pageTextNotContains('External links are not allowed in this menu.');
    }
  }

  /**
   * Asserts that creating/editing a menu item with a given link is disallowed.
   *
   * @param string $link
   *   Link of the menu item (URL, path, <nolink, <front>, etc).
   */
  public function assertMenuLinkDisallowed($link) {
    $paths = [
      // Add menu item.
      'admin/structure/menu/manage/' . $this->menu->id() . '/add',
      // Edit existing menu item.
      'admin/structure/menu/item/' . $this->menuItem->id() . '/edit',
    ];
    foreach ($paths as $path) {
      $this->drupalGet($path);
      $this->submitForm([
        'title' => 'My menu item',
        'link[0][uri]' => $link,
      ], 'Save');
      $this->assertSession()->statusCodeEquals(200);
      $this->assertSession()->pageTextContains('External links are not allowed in this menu.');
    }
  }

}
